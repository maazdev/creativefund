<?php

/**
 * Email Addresses (plain)
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails/Plain
 * @version     3.2.1
 */
if ( !defined( 'ABSPATH' ) )
	exit; // Exit if accessed directly
echo "\n" . __( 'Billing address', 'lifeline' ) . ":\n";
echo $order->get_formatted_billing_address() . "\n\n";
if ( get_option( 'woocommerce_ship_to_billing_address_only' ) == 'no' && ( $shipping = $order->get_formatted_shipping_address() ) ) :
	echo __( 'Shipping address', 'lifeline' ) . ":\n";
	echo $shipping . "\n\n";
endif;
