<?php
if (!defined('ABSPATH')) {
    exit;
}
if (!defined('ABSPATH')) {
    exit;
}
return array(
    'enabled' => array(
        'title' => __('PayPal Payment Gateway', 'eh-paypal-express'),
        'label' => __('Enable', 'eh-paypal-express'),
        'type' => 'checkbox',
        'description' => __('This option will enable PayPal payment method in checkout page.', 'eh-paypal-express'),
        'default' => 'no',
        'desc_tip' => true
    ),
    'title' => array(
        'title' => __('Title', 'eh-paypal-express'),
        'type' => 'text',
        'description' => __('Enter the title of the checkout which the user can see.', 'eh-paypal-express'),
        'default' => __('PayPal', 'eh-paypal-express'),
        'desc_tip' => true
    ),
    'description' => array(
        'title' => __('Regular Description', 'eh-paypal-express'),
        'type' => 'textarea',
        'css' => 'width:25em',
        'description' => __('Description which the user sees during checkout.', 'eh-paypal-express'),
        'default' => __('Secure payment via PayPal.', 'eh-paypal-express'),
        'desc_tip' => true
    ),
    'credentials_title' => array(
        'title' => sprintf(__('<span style="text-decoration: underline;color:brown;">PayPal Credentials<span>', 'eh-paypal-express')),
        'type' => 'title'
    ),
    'environment' => array(
        'title' => __('Environment', 'eh-paypal-express'),
        'type' => 'select',
        'class'       => 'wc-enhanced-select',
        'options' => array(
            'sandbox' => __('Sandbox Mode', 'eh-paypal-express'),
            'live' => __('Live Mode', 'eh-paypal-express')
        ),
        'description' => sprintf(__('<div id="environment_alert_desc"></div>', 'eh-paypal-express')),
        'default' => 'sandbox'
    ),
    'sandbox_username' => array(
        'title' => __('Sandbox API Username', 'eh-paypal-express'),
        'type' => 'text',
        'default' => ''
    ),
    'sandbox_password' => array(
        'title' => __('Sandbox API Password', 'eh-paypal-express'),
        'type' => 'password',
        'default' => ''
    ),
    'sandbox_signature' => array(
        'title' => __('Sandbox API Signature', 'eh-paypal-express'),
        'type' => 'password',
        'default' => ''
    ),
    'live_username' => array(
        'title' => __('Live API Username', 'eh-paypal-express'),
        'type' => 'text',
        'default' => ''
    ),
    'live_password' => array(
        'title' => __('Live API Password', 'eh-paypal-express'),
        'type' => 'password',
        'default' => ''
    ),
    'live_signature' => array(
        'title' => __('Live API Signature', 'eh-paypal-express'),
        'type' => 'password',
        'default' => ''
    ),
    'express_title' => array(
        'title' => sprintf(__('<span style="text-decoration: underline;color:brown;">PayPal Express Checkout<span>', 'eh-paypal-express')),
        'type' => 'title'
    ),
    'express_enabled' => array(
        'title' => __('PayPal Express', 'eh-paypal-express'),
        'label' => __('Enable', 'eh-paypal-express'),
        'type' => 'checkbox',
        'description' => __('Enable PayPal Express Gateway option.', 'eh-paypal-express'),
        'default' => 'no',
        'desc_tip' => true
    ),    
    'credit_checkout' => array(
        'title' => __('Credit Card Checkout', 'eh-paypal-express'),
        'type' => 'checkbox',
        'label' => __('Enable', 'eh-paypal-express'),
        'default' => 'no',
        'description' => __('Check to allow customer to pay using their credit card instead of PayPal Account.', 'eh-paypal-express'),
        'desc_tip' => true
    ),
    'button_size' => array(
        'title'       => __( 'Button Size', 'eh-paypal-express' ),
        'type'        => 'select',
        'class'       => 'wc-enhanced-select',
        'description' => __( 'Select the Button size that fits your shop theme.', 'eh-paypal-express' ),
        'default'     => 'medium',
        'desc_tip'    => true,
        'options'     => array(
                'small'  => __( 'Small', 'eh-paypal-express' ),
                'medium' => __( 'Medium', 'eh-paypal-express' ),
                'large'  => __( 'Large', 'eh-paypal-express' )
        )
    ),
    'express_description' => array(
        'title' => __('Express Description', 'eh-paypal-express'),
        'type' => 'textarea',
        'css' => 'width:25em',
        'description' => __('Description which the user sees during PayPal Express.', 'eh-paypal-express'),
        'default' => __('Reduce multiple click by clicking on PayPal', 'eh-paypal-express'),
        'desc_tip' => true
    ),
    'express_on_cart_page' => array(
        'title' => __('Cart Page Checkout', 'eh-paypal-express'),
        'type' => 'checkbox',
        'label' => __('Enable Express', 'eh-paypal-express'),
        'default' => 'no',
        'description' => __('Allows customers to checkout using PayPal directly from a cart page.', 'eh-paypal-express'),
        'desc_tip' => true        
    ),
    'abilities_title' => array(
        'title' => sprintf(__('<span style="text-decoration: underline;color:brown;">PayPal Abilities<span>', 'eh-paypal-express')),
        'type' => 'title'
    ),    
    'business_name' => array(
        'title' => __('Business Name', 'eh-paypal-express'),
        'type' => 'text',
        'description' => __('Enter the Business name to display in PayPal Checkout Page.', 'eh-paypal-express'),
        'default' => __(get_bloginfo( 'name', 'display'), 'eh-paypal-express'),
        'desc_tip' => true
    ),
    'payment_action' => array(
        'title' => __('Payment Action', 'eh-paypal-express'),
        'type' => 'select',
        'class'       => 'wc-enhanced-select',
        'options' => array(
            'sale' => __('Sale', 'eh-paypal-express')
        ),
        'description' => sprintf(__('Select whether you want to capture the payment or not.', 'eh-paypal-express')),
        'default' => 'sale',
        'desc_tip' =>true
    ),    
    'paypal_allow_override' => array(
        'title' => __('Override Addresses ', 'eh-paypal-express'),
        'type' => 'checkbox',
        'label' => __('Do you let buyers override their PayPal addresses? ', 'eh-paypal-express'),
        'default' => 'no',
        'description' => __('Check to allow buyers override their PayPal addresses. - <i>Enabling this will affect express checkout and PayPal will strictly verify the address</i>', 'eh-paypal-express'),
        'desc_tip' => false
    ),
    'paypal_locale' => array(
        'title' => __('PayPal Locale', 'eh-paypal-express'),
        'type' => 'checkbox',
        'label' => __('Use Store Locale', 'eh-paypal-express'),
        'default' => 'yes',
        'description' => __('Check to set your store locale code to PayPal page locale.', 'eh-paypal-express'),
        'desc_tip' => true
    ),
    'landing_page' => array(
        'title' => __('Landing Page', 'eh-paypal-express'),
        'type' => 'select',
        'class'       => 'wc-enhanced-select',
        'options' => array(
            'login' => __('Login', 'eh-paypal-express'),
            'billing' => __('Billing', 'eh-paypal-express')
        ),
        'description' => sprintf(__('PayPal Page which is used to display as default.', 'eh-paypal-express')),
        'default' => 'billing',
        'desc_tip' =>true
    ),        
    'customer_service' => array(
        'title' => __('Customer Service Number', 'eh-paypal-express'),
        'type' => 'text',
        'description' => __('Enter the customer service number which will be displayed in PayPal Review Page.', 'eh-paypal-express'),
        'default' => __('', 'eh-paypal-express'),
        'desc_tip' => true
    ),
    'checkout_logo' => array(
        'title' => __('PayPal Checkout Logo', 'eh-paypal-express'),
        'type' => 'text',
        'description' => __('Enter URL of the image to be displayed as logo in PayPal Checkout Page. Image URL should be of SSL Host URL. Use <a href="http://www.sslpic.com" targer="_blank"> SSL Host</a>.','eh-paypal-express'). sprintf('<br><br><img src="%s" width="190px" height="90px" style="cursor:pointer" title="PayPal Checkout Logo">',('' == $this->get_option('checkout_logo'))?EH_PAYPAL_MAIN_URL.'assets/img/nopreview.jpg':$this->get_option('checkout_logo')),
        'placeholder' => 'Image Size ( 190 x 90 )px'
    ),
    'checkout_banner' => array(
        'title' => __('PayPal Checkout Banner', 'eh-paypal-express'),
        'type' => 'text',
        'description' => __('Enter URL of the image to be displayed as banner in PayPal Checkout Page. Image URL should be of SSL Host URL. Use <a href="http://www.sslpic.com" targer="_blank"> SSL Host</a>.','eh-paypal-express'). sprintf('<br><br><img src="%s" width="750px" height="90px" style="cursor:pointer" title="PayPal Checkout Banner">',('' == $this->get_option('checkout_banner'))?EH_PAYPAL_MAIN_URL.'assets/img/nopreview.jpg':$this->get_option('checkout_banner')),
        'placeholder' => 'Image Size ( 750 x 90 )px'
    ),    
    'review_title' => array(
        'title' => sprintf(__('<span style="text-decoration: underline;color:brown;">Review Page<span>', 'eh-paypal-express')),
        'type' => 'title'
    ),
    'skip_review' => array(
        'title' => __('Skip Review Page', 'eh-paypal-express'),
        'type' => 'checkbox',
        'label' => __('Enable', 'eh-paypal-express'),
        'default' => 'yes',
        'description' => __('Check to skip the review page for the customer to complete order.', 'eh-paypal-express'),
        'desc_tip' => true
    ),        
    'policy_notes' => array(
        'title' => __('Seller Policy', 'eh-paypal-express'),
        'type' => 'textarea',
        'css' => 'width:25em',
        'description' => __('Enter the seller protection policy or customized text which will be displayed in order review page.', 'eh-paypal-express'),
        'default' => __('You are Protected by '.get_bloginfo( 'name', 'display').' Policy', 'eh-paypal-express'),
        'desc_tip' => true
    ),
    'log_title' => array(
        'title' => sprintf(__('<span style="text-decoration: underline;color:brown;">Developer Settings<span>', 'eh-paypal-express')),
        'type' => 'title',
        'description' => sprintf(__('Enable Logging to save PayPal payment logs into log file. <a href="'.admin_url("admin.php?page=wc-status&tab=logs").'" target="_blank"> Check Now </a>', 'eh-paypal-express'))
    ),
    'paypal_logging' => array(
        'title' => __('Logging', 'eh-paypal-express'),
        'label' => __('Enable', 'eh-paypal-express'),
        'type' => 'checkbox',
        'description' => sprintf(__('<span style="color:green">Log File</span>: ' . strstr(wc_get_log_file_path('eh_paypal_express_log'), 'eh_paypal_express_log') . ' ( ' . $this->file_size(filesize(wc_get_log_file_path('eh_paypal_express_log'))) . ' ) ', 'eh-paypal-express')),
        'default' => 'yes'
    ),
    'ipn_url' => array(
        'title' => __('Override IPN URL', 'eh-paypal-express'),
        'type' => 'text',
        'description' => __('Enter override IPN URL to capture PayPal IPN Response.', 'eh-paypal-express'),
        'default' => __('', 'eh-paypal-express'),
        'placeholder'=> 'IPN URL',
        'desc_tip' => true
    )
    );

