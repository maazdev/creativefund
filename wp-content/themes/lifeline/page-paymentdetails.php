<?php /* Template Name: PaymentDetail*/ ?>
 
<?php get_header(); ?>
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "creativefund";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

?>
<div class="container">
<?php
$sql = "SELECT SUM(amount) AS Totalamount FROM transactions";
//var_dump($sql);
$res = mysqli_query($conn, $sql);
//var_dump($res);
$row = mysqli_fetch_assoc($res); 
echo "TotalAmount" . $row['Totalamount'];

$sql = "SELECT SUM(amount) AS Totalamountstripe FROM transactions where type='stripe'";
//var_dump($sql);
$res = mysqli_query($conn, $sql);
//var_dump($res);
$row = mysqli_fetch_assoc($res); 
echo "TotalAmountStripe" . $row['Totalamountstripe'];

$sql = "SELECT SUM(amount) AS Totalamountpaypal FROM transactions where type='paypal'";
//var_dump($sql);
$res = mysqli_query($conn, $sql);
//var_dump($res);
$row = mysqli_fetch_assoc($res); 
echo "TotalAmountPaypal" . $row['Totalamountpaypal'];

?>
<table id="example" class="display" style="width:100%">
        <thead>
            <tr>
				<th>transaction Id</th>
				<th>date</th>
                <th>Doner Name</th>
				<th>organization name</th>
				<th>type</th>
                <th>amount</th>  
              
            </tr>
        </thead>
        <tfoot>
            <tr>
				<th>transaction Id</th>
				<th>date</th>
                <th>Doner Name</th>
				<th>organization name</th>
				<th>type</th>
                <th>amount</th>  
            </tr>
        </tfoot>
    </table>
	</div>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script>
		$(document).ready(function() {
    $('#example').DataTable( {
        "ajax": "http://localhost/creativefund/wp-content/themes/lifeline/jsonp.php",
        "columns": [
			{ "data": "id" },
			 { "data": "date" },
            { "data": "doner_name" },
			 { "data": "organization_name" },
			 { "data": "type" },
            { "data": "amount" , render: $.fn.dataTable.render.number( ',', '.', 0, '$' )}
            
        ]
    } );
} );
	</script>

<?php get_footer(); ?>