=== PayPal Express Checkout Payment Gateway for WooCommerce ===
Contributors: xadapter, mujeebur
Donate link: 
Tags: paypal, express checkout, payment, credit card, woocommerce
Requires at least: 3.0.1
Tested up to: 4.9.5
Stable tag: 1.2.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

PayPal Express Checkout Payment Gateway for WooCommerce - Pay via Credit Card, PayPal using PayPal Express Checkout.

== Description ==

= Introduction =
Do you want the fastest solution to accept Credit Cards, PayPal on your website? Our PayPal Express Checkout Payment Gateway for WooCommerce lets you accept credit card payments and PayPal payments through PayPal secure payment gateway.

With this plugin, your customer can use their credit cards or PayPal Money to make order from cart page itself. This means a smoother experience for your users as they never have to leave your website for making payments.

= Features =
* Reduce the clicks by enabling PayPal Express Checkout.
* Pay using Credit Card through PayPal.
* Pay using PayPal Money.
* Complete order from cart page. Premium version supports even Product Page!
* Customize each user facing elements by various settings options.
* Faster Checkout by skipping order review page.

<blockquote>

= Premium version Features =

<ul>
<li> - Add Express PayPal Checkout Option on Product Page for Faster Checkout.</li>
<li> - Capture the authorized payment later.</li>
<li> - Partial and Full Refund the order amount directly from Order Admin Page.</li>
<li> - Lots of Customization Options like Button Style, Position, Etc.</li>
<li> - Add Invoice Prefix to identify Store order.</li>
<li> - Option to enable In-Context checkout, to keep customers inside your store while checkout process.</li>   
<li> - Timely compatibility updates and bug fixes.</li>
<li> - Premium support!</li>
</ul>

For complete list of features and details, please visit <a rel="nofollow" href="https://www.xadapter.com/product/paypal-express-checkout-payment-gateway-woocommerce/">PayPal Express Checkout Payment Gateway for WooCommerce</a>
</blockquote>

= About XAdapter =

XAdapter is a reliable, efficient and focused WooCommerce extension developer firm. Our team comprises of profoundly experienced developers with a vast knowledge pool.

== Installation ==

1. Upload the plugin folder to the '/wp-content/plugins/' directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. Thats it! you can now configure the plugin.

== Frequently Asked Questions ==

= Is the plugin configuration complicated? =
The plugin is very easy to configure. We have a step by step tutorial on setting up this plugin. Our Help Desk also has extensive documentation which includes FAQs, Troubleshooting Guide, Knowledge Base and Code snippets. Please [visit](https://www.xadapter.com/support/forum/paypal-express-checkout-payment-gateway-woocommerce/) if you need any help. 

== Screenshots ==

1. PayPal Express Checkout Settings
2. Cart Page - Express Checkout Option
3. Order Review Page

== Changelog ==

= 1.2.3 =
 * Fix: Orders are placed with paypal email instead of billing email.
 * Compatibility  Tested OK with WC3.3.5 and WP4.9.5

= 1.2.2 =
 * Fix: Checkout field validation updated.
= 1.2.1 =
 * Fix: Checkout field validation.

= 1.2.0 =
 * Fix: Checkout field validation.
 * Fix: Tax amount rounding
 * Compatibility  Tested OK with WC3.3.1 and WP4.9.4

= 1.1.9 =
 * Fix: PayPal address override default to false.

= 1.1.7 =
 * Fix: Create new account on checkout.

= 1.1.7 =
 * Compatibility  Tested OK with WC3.2.5 and WP4.9

= 1.1.6 =
 * Compatibility issue from PHP 5.4 and 5.3

= 1.1.5 =
 * Minor Content Changed.

= 1.1.4 =
 * Marketing Content Changed.

= 1.1.3 =
 * Minor Content Changed.

= 1.1.2 =
 * Minor Content Changed.

= 1.1.1 =
 * Code changes for WC2.7RC (Order Function).

= 1.1.0 =
 * WooCommerce V2.7 RC and Upgrade Supports.

= 1.0.2 =
 * Version Tested.

= 1.0.1 =
 * Bug Fixed.
 * Periodic Update.

= 1.0 =
 * Both PayPal Express and Regular checkout.
 * Order Overview Page skipping.
 * Customize your PayPal Transactions.
 
== Upgrade Notice ==

= 1.2.3 =
 * Fix: Orders are placed with paypal email instead of billing email.
 * Compatibility  Tested OK with WC3.3.5 and WP4.9.5