<?php
if (!defined('ABSPATH')) {
    exit;
}
class Eh_PE_Request_Built
{
    protected $params=array();
    const version="115";
    public $supported_decimal_currencies = array('HUF', 'JPY', 'TWD');
    public $store_currency;
    public $http_version;
    function __construct($username, $password, $signature)
    {
        $this->store_currency = get_woocommerce_currency();
        $this->make_params(array(
            'USER'      => $username,
            'PWD'       => $password,
            'SIGNATURE' => $signature,
            'VERSION'   => self::version
        ));
        $this->http_version = '1.1';
    }
    public function make_request_params(array $args)
    {
        $this->make_params
                (
                    array
                    (
                        'METHOD'                => $args['method'],
                        'RETURNURL'             => $args['return_url'],
                        'CANCELURL'             => $args['cancel_url'],
                        'ADDROVERRIDE'          => $args['address_override'],
                        'BRANDNAME'             => $args['business_name'],
                        'HDRIMG'                => $args['banner'],
                        'LOGOIMG'               => $args['logo'],
                        'SOLUTIONTYPE'          => 'Sole',
                        'CUSTOMERSERVICENUMBER' => $args['customerservicenumber'],
                        'LOCALECODE'            => $args['localecode'],
                    )
                );
        if($args['credit'])
        {
            $this->make_params
                (
                    array
                    (
                        'USERSELECTEDFUNDINGSOURCE' => 'CreditCard',
                        'LANDINGPAGE'               => 'Billing',
                    )
                );
        }
        else
        {
            $this->make_params
                (
                    array
                    (
                        'LANDINGPAGE' => $args['landing_page']
                    )
                );
        }
        $i=0;
        if (!defined('WOOCOMMERCE_CART')) {
            define('WOOCOMMERCE_CART', true);
        }
        WC()->cart->calculate_totals();
        $cart_item=wc()->cart->get_cart();
        foreach ($cart_item as $item) 
        {
            $cart_product       = $item['data'];
            $line_item_title    = $cart_product->get_title();
            $desc_temp          = array();
            foreach ($item['variation'] as $key => $value) 
            {
                $desc_temp[]    = wc_attribute_label(str_replace('attribute_','',$key)).' : '.$value;
            }
            $line_item_desc     = implode(', ', $desc_temp);
            $line_item_quan     = $item['quantity'];
            $line_item_total    = $item['line_subtotal']/$line_item_quan;
            $line_item_url      = $cart_product->get_permalink();
            $this->add_line_items
                    (
                        array
                        (
                            'NAME'      => $line_item_title,
                            'DESC'      => $line_item_desc,
                            'AMT'       => $this->make_paypal_amount($line_item_total),
                            'QTY'       => $line_item_quan,
                            'ITEMURL'   => $line_item_url
                        ),
                        $i++
                    );
        }
        if (WC()->cart->get_cart_discount_total() > 0) 
        {
            $this->add_line_items
                    (
                        array
                        (
                            'NAME'  => 'Discount',
                            'DESC'  => implode(', ', wc()->cart->get_applied_coupons()),
                            'QTY'   => 1,
                            'AMT'   => - $this->make_paypal_amount(WC()->cart->get_cart_discount_total()),
                        ),
                        $i++
                    );
        }
        $this->add_payment_params
                (
                    array
                    (
                        'AMT'                   => $this->make_paypal_amount(WC()->cart->total),
                        'CURRENCYCODE'          => $this->store_currency,
                        'ITEMAMT'               => $this->make_paypal_amount(WC()->cart->cart_contents_total + WC()->cart->fee_total),
                        'SHIPPINGAMT'           => $this->make_paypal_amount(WC()->cart->shipping_total),
                        'TAXAMT'                => wc_round_tax_total(WC()->cart->tax_total + WC()->cart->shipping_tax_total),
                        'NOTIFYURL'             => $args['notify_url'],
                        'PAYMENTACTION'         => 'Sale'
                    )
                );
        $this->make_param('MAXAMT',$this->make_paypal_amount(WC()->cart->total + ceil(WC()->cart->total * 0.75)));
        if (is_user_logged_in()) {
            $customer_id = get_current_user_id();
            $this->add_payment_params
                    (
                        array
                        (
                            'SHIPTONAME' => get_user_meta($customer_id, 'shipping_first_name', true) . ' ' . get_user_meta($customer_id, 'shipping_last_name', true),
                        )
                    );
        }
        $this->add_payment_params
                (
                    array
                    (
                        'SHIPTOSTREET'      => WC()->customer->get_shipping_address(),
                        'SHIPTOSTREET2'     => WC()->customer->get_shipping_address_2(),
                        'SHIPTOCITY'        => WC()->customer->get_shipping_city(),
                        'SHIPTOSTATE'       => WC()->customer->get_shipping_state(),
                        'SHIPTOZIP'         => WC()->customer->get_shipping_postcode(),
                        'SHIPTOCOUNTRYCODE' => WC()->customer->get_shipping_country(),
                        'SHIPTOPHONENUM'    => ( WC()->session->post_data['billing_phone'] ) ? WC()->session->post_data['billing_phone'] : WC()->session->post_data['billing_phone'],
                        'NOTETEXT'          => ( WC()->session->post_data['order_comments'] ) ? WC()->session->post_data['order_comments'] : WC()->session->post_data['order_comments'],
                    )
                );
        Eh_PayPal_Log::log_update($this->params,'Setting Express Checkout');
        return $this->get_params();
    }
    public function get_checkout_details(array $args)
    {
        $this->make_params($args);
        Eh_PayPal_Log::log_update($this->params,'Getting Express Checkout Details');
        return $this->get_params();
    }
    public function finish_request_params(array $args,$order)
    {
        $this->make_params
                (
                    array
                    (
                        'METHOD'        => $args['method'],
                        'TOKEN'         => $args['token'],
                        'PAYERID'       => $args['payer_id'],
                        'BUTTONSOURCE'  => $args['button']
                    )
                );
        $order_item=$order->get_items();
        $i=0;
        $currency=(WC()->version < '2.7.0')?$order->get_order_currency():$order->get_currency();
        $item_total=0;
        foreach ($order_item as $item)
        {
            $line_item_title    = $item['name'];
            $desc_temp          = array();
            foreach ($item as $key => $value) 
            {
                if(strstr($key, 'pa_'))
                {
                    $desc_temp[] = wc_attribute_label($key).' : '.$value;
                }
            }
            $line_item_desc     = implode(', ', $desc_temp);
            $line_item_quan     = $item['qty'];
            $line_item_total    = $item['line_subtotal']/$line_item_quan;
            $this->add_line_items
                    (
                        array
                        (
                            'NAME'      => $line_item_title,
                            'DESC'      => $line_item_desc,
                            'AMT'       => $this->make_paypal_amount($line_item_total,$currency),
                            'QTY'       => $line_item_quan,
                        ),
                        $i++
                    );
            $item_total+=$order->get_item_total($item)*$line_item_quan;
        }
        if ($order->get_total_discount() > 0) 
        {
            $this->add_line_items
                    (
                        array
                        (
                            'NAME'  => 'Discount',
                            'DESC'  => implode(', ', $order->get_used_coupons()),
                            'QTY'   => 1,
                            'AMT'   => - $this->make_paypal_amount($order->get_total_discount()),
                        ),
                        $i++
                    );
        }
        $this->add_payment_params
                (
                    array
                    (
                        'AMT'               => $this->make_paypal_amount($order->get_total(),$currency),
                        'CURRENCYCODE'      => $currency,
                        'ITEMAMT'           => $this->make_paypal_amount($item_total,$currency),
                        'SHIPPINGAMT'       => $this->make_paypal_amount($order->get_total_shipping(),$currency),
                        'TAXAMT'            => $this->make_paypal_amount($order->get_total_tax(),$currency),
                        'PAYMENTREQUESTID'  => (WC()->version < '2.7.0')?$order->id:$order->get_id(),
                        'PAYMENTACTION'     => 'Sale'
                    )
                );
        Eh_PayPal_Log::log_update($this->params,'Processing Express Checkout');
        return $this->get_params();
    }
    public function make_capture_params($args)
    {
        $this->make_params
                (
                    array
                    (
                        'METHOD'            => $args['method'],
                        'AUTHORIZATIONID'   => $args['auth_id'],
                        'AMT'               => $this->make_paypal_amount($args['amount'],$args['currency']),
                        'CURRENCYCODE'      => $args['currency'],
                        'COMPLETETYPE'      => $args['type']
                    )
                );
        Eh_PayPal_Log::log_update($this->params,'Capture Express Checkout');
        return $this->get_params();
    }
    public function make_refund_params($args)
    {
        $this->make_params
                (
                    array
                    (
                        'METHOD'            => $args['method'],
                        'TRANSACTIONID'     => $args['auth_id'],
                        'AMT'               => $this->make_paypal_amount($args['amount'],$args['currency']),
                        'CURRENCYCODE'      => $args['currency'],
                        'REFUNDTYPE'        => $args['type']
                    )
                );
        Eh_PayPal_Log::log_update($this->params,'Refund Express Checkout');
        return $this->get_params();
    }
    public function query_params()
    {
        foreach ($this->params as $key => $value) 
        {
            if ('' === $value || is_null($value)) {
                unset($this->params[$key]);
            }
            if (false !== strpos($key, 'AMT')) 
            {
                if (isset($this->params['PAYMENTREQUEST_0_CURRENCYCODE']) && 'USD' == $this->params['PAYMENTREQUEST_0_CURRENCYCODE'] && $value > 10000)
                {
                    wc_add_notice(sprintf('%1$s amount of $%2$s must be less than $10,000.00', 'PayPal Amount', $value), 'error');
                    wp_redirect(wc_get_cart_url());
                    exit;
                }
                $this->params[$key] = number_format($value, 2, '.', '');
            }
        }        
        return $this->params;
    }
    public function get_params() {
        $args = array(
            'method' => 'POST',
            'timeout' => 60,
            'redirection' => 0,
            'httpversion' => $this->http_version,
            'sslverify' => FALSE,
            'blocking' => true,
            'user-agent' => 'EH_PAYPAL_EXPRESS_CHECKOUT',
            'headers' => array(),
            'body' => http_build_query($this->query_params()),
            'cookies' => array(),
        );
        return $args;
    }
    public function make_paypal_amount($amount,$currency='')
    {
        $currency=  empty($currency)?$this->store_currency:$currency;
        if (in_array($currency, $this->supported_decimal_currencies))
        {
            return round((float) $amount, 0);
        }
        else
        {
            return round((float) $amount, 2);
        }
    }
    public function add_line_items($items,$count)
    {
        foreach ($items as $line_key => $line_value) {
            $this->make_param("L_PAYMENTREQUEST_0_{$line_key}{$count}", $line_value);
        }
    }
    public function add_payment_params($items)
    {
        foreach ($items as $item_key => $item_value) {
            $this->make_param("PAYMENTREQUEST_0_{$item_key}", $item_value);
        }
    }
    public function make_param($key,$value) {
        $this->params[$key] = $value;
    }
    public function make_params(array $args) {
        foreach ($args as $key => $value) {
            $this->params[$key] = $value;
        }
    }
}
