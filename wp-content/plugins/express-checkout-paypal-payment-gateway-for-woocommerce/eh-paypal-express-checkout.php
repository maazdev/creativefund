<?php
/*
 * Plugin Name: PayPal Express Checkout Payment Gateway for WooCommerce ( Basic )
 * Plugin URI: https://www.xadapter.com/product/paypal-express-checkout-payment-gateway-woocommerce/
 * Description: Simplify your Store's checkout Process using PayPal Express Checkout.
 * Author: XAdapter
 * Author URI: https://www.xadapter.com/shop
 * Version: 1.2.3
 * WC tested up to: 3.3.5
 */
if (!defined('ABSPATH')) {
    exit;
}
if (!defined('EH_PAYPAL_MAIN_PATH')) {
    define('EH_PAYPAL_MAIN_PATH', plugin_dir_path(__FILE__));
}
if (!defined('EH_PAYPAL_MAIN_URL')) {
    define('EH_PAYPAL_MAIN_URL', plugin_dir_url(__FILE__));
}
if (!defined('EH_PAYPAL_VERSION')) {
    define('EH_PAYPAL_VERSION', '1.2.3');
}

require_once(ABSPATH . "wp-admin/includes/plugin.php");
// Change the Pack IF BASIC  mention switch('BASIC') ELSE mention switch('PREMIUM')
switch('BASIC')
{
    case 'PREMIUM':
        $conflict   = 'basic';
        $base       = 'premium';
        break;
    case 'BASIC':
        $conflict   = 'premium';
        $base       = 'basic';
        break;
}
// Enter your plugin unique option name below $option_name variable
$option_name='eh_paypal_express_pack';

if(get_option($option_name)==$conflict)
{
    add_action('admin_notices','eh_wc_admin_notices', 99);
    deactivate_plugins(plugin_basename(__FILE__));
    function eh_wc_admin_notices()
    {
        is_admin() && add_filter('gettext', function($translated_text, $untranslated_text, $domain)
        {
            $old = array(
                "Plugin <strong>activated</strong>.",
                "Selected plugins <strong>activated</strong>."
            );
            $error_text='';
            // Change the Pack IF BASIC  mention switch('BASIC') ELSE mention switch('PREMIUM')
            switch('BASIC')
            {
                case 'PREMIUM':
                    $error_text="BASIC Version of this Plugin Installed. Please uninstall the BASIC Version before activating PREMIUM.";
                    break;
                case 'BASIC':
                    $error_text="PREMIUM Version of this Plugin Installed. Please uninstall the PREMIUM Version before activating BASIC.";
                    break;
            }
            $new = "<span style='color:red'>".$error_text."</span>";
            if (in_array($untranslated_text, $old, true)) {
                $translated_text = $new;
            }
            return $translated_text;
        }, 99, 3);
    }
    return;
}
else
{
    update_option($option_name, $base);	
    register_deactivation_hook(__FILE__, 'eh_paypal_express_deactivate_work');
    // Enter your plugin unique option name below update_option function
    function eh_paypal_express_deactivate_work()
    {
        update_option('eh_paypal_express_pack', '');
    }
    if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
            register_activation_hook(__FILE__, 'eh_paypal_express_init_log');
            include(EH_PAYPAL_MAIN_PATH . "includes/log.php");
            add_filter('plugin_action_links_' . plugin_basename(__FILE__), 'eh_paypal_express_plugin_action_links');
            function eh_paypal_express_plugin_action_links($links)
            {
                $setting_link = admin_url('admin.php?page=wc-settings&tab=checkout&section=eh_paypal_express');
                $plugin_links = array(
                    '<a href="' . $setting_link . '">' . __('Settings', 'eh-paypal-express') . '</a>',
                    '<a href="https://www.xadapter.com/product/paypal-express-checkout-payment-gateway-woocommerce/" target="_blank">' . __( 'Premium Upgrade', 'eh-paypal-express' ) . '</a>',
                    '<a href="https://www.xadapter.com/support/forum/paypal-express-checkout-payment-gateway-woocommerce/" target="_blank">' . __('Support', 'eh-paypal-express') . '</a>',
                    '<a href="https://wordpress.org/support/plugin/express-checkout-paypal-payment-gateway-for-woocommerce/reviews/" target="_blank">' . __('Review', 'eh-paypal-express') . '</a>',
                );
                return array_merge($plugin_links, $links);
            }
            function eh_paypal_express_run()
            {
                static $eh_paypal_plugin;
                if(!isset($eh_paypal_plugin))
                {
                    require_once (EH_PAYPAL_MAIN_PATH . "includes/class-eh-paypal-init-handler.php");
                    $eh_paypal_plugin=new Eh_Paypal_Express_Handlers();
                }
                return $eh_paypal_plugin;
            }
            eh_paypal_express_run()->express_run();

        } else {
            add_action('admin_notices', 'eh_paypal_express_wc_admin_notices', 99);
            deactivate_plugins(plugin_basename(__FILE__));
        }  
    function eh_paypal_express_wc_admin_notices()
    {
        is_admin() && add_filter('gettext', function($translated_text, $untranslated_text, $domain)
        {
            $old = array(
                "Plugin <strong>activated</strong>.",
                "Selected plugins <strong>activated</strong>."
            );
            $new = "<span style='color:red'>PayPal Express Payment for WooCommerce (ExtensionHawk)-</span> Plugin Needs WooCommerce to Work.";
            if (in_array($untranslated_text, $old, true)) {
                $translated_text = $new;
            }
            return $translated_text;
        }, 99, 3);
    }
    function eh_paypal_express_init_log()
    {
        if(WC()->version >= '2.7.0')
        {
            $log      = wc_get_logger();
            $init_msg = Eh_PayPal_Log::init_log();
            $context = array( 'source' => 'eh_paypal_express_log' );
            $log->log("debug", $init_msg,$context);
        }
        else
        {
            $log      = new WC_Logger();
            $init_msg = Eh_PayPal_Log::init_log();
            $log->add("eh_paypal_express_log", $init_msg);
        }
    }
}
